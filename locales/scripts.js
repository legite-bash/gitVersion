/*! ./locales/scripts.js*/

/*! - /www/git/intersites/lib/legral/php/gestLib/gestLib.js - */
/*!
fichier: gestLib.js
auteur:pascal TOLEDo
date de creation : 2012.02.21
date d emodification: 2014.08.08
source: http://gitorious.org/gestLib
*/

GESTLIBVERSION= '2.0.1';

// gestionnaire de la box des donnees
function gestionLibrairie_dataBox(o){
//	this.nom=o.nom;		//facultatif
	this.idHTML=o.idHTML;	//facultatif
	this.idCSS=null;
	this.idCSSParent=o.idCSSParent;	// obligatoire
	this.className='gestLibDataBox';
	if(o.className)this.className+=' '+o.className;

	this.genre=(o.genre=='TEXTAREA')?'TEXTAREA':'DIV';	// 'TEXTAREA' sinon DIV par defaut
	return this;
}

gestionLibrairie_dataBox.prototype={
	create:function(){
		this.idCSS=document.createElement(this.genre.toLowerCase());
		this.idCSSParent.appendChild(this.idCSS);
		this.init();
		return this.idCSS;
	}
	// - 're)initialisation du contenaire - //
	,init:function(){
		this.idCSS.id=this.idHTML;
		this.idCSS.className=this.className;
		this.idCSS.style.display='block';
		return null;
	}

	// o: object
	// o.ALL: texte qui sera ecrit quelque soit le genre de dataBox
	// o.DIV: texte ecrit uniquement si dataBox.genre == DIV
	// o.TEXTAREA: texte ecrit uniquement si dataBox.genre == TEXTAREA
	// o.notWrite: n'ecrit pas dans la console mais retroune seulement le texte formater

	,write:function(o){
//		if(typeof(o)!='object)')return null;
		var str=o;	// par defaut o est supposé un string
		if(o.ALL)str=o.ALL;	// surcharge si toute destination preciser
		var br='';
		var isObj=(typeof(o)=='object');
		if(this.genre=='TEXTAREA'){
			if(!o.ALL && !o.TEXTAREA){return '';}
			if(isObj && o.TEXTAREA)str=o.TEXTAREA;	// surcharge si specifier
			br=(o.br!=undefined)?o.br:'\n';
			str+=br;
			if(!o.notWrite){
				if(o.clear===1)
					this.idCSS.value=str
				else this.idCSS.value+=str;
			}
		}
		if(this.genre=='DIV'){
			if(!o.ALL && !o.DIV){return '';}
			if(isObj && o.DIV)str=o.DIV;	// surcharge si specifier
			br=(o.br!=undefined)?o.br:'<br>';
			str+=br;
			if(!o.notWrite){
				if(o.clear===1)
					this.idCSS.innerHTML=str
				else this.idCSS.innerHTML+=str;
			}
		}
		return str;
	}


}


//options: nom:obligatoire
//erreurNo:
// -1: absence de nom
// -10..-11..-12: absence de div pour la console
// 100+ specifique a la lib appellé
function gestionLibrairie_lib(options)
	{
	if((options.nom==undefined)&&((options.idHTML==undefined)))return null;	// l'un ou l'autre doit etre definit
	this.erreurNo=0;
	this.erreurTxt=new Array();//attention tableau de tableau
	if (options.nom==undefined){this.erreurNo=-1;}
	this.date=new Date();
	this.libType=(options.libType=='tiers')?'tiers':'perso';	// defaut:pas de console
	this.isConsole=(options.isConsole===1)?1:0;	// defaut:pas de console
	this.consoleGenre=null;	// TEXTAREA,DIV
	this.isVisible=(options.isVisible===0)?0:1;	// defaut:visible
	this.nom=   (options.nom   !=undefined)?options.nom   :options.idHTML;

	// - console - //
	this.idCSSSup=null;	this.idCSSNom=null;	this.idCSSTxt=null;
	this.idHTML=(options.idHTML!=undefined)?options.idHTML:options.nom;
	this.idHTMLMenus=this.idHTML+'_menus';
	this.idCSSMenus=null;	// support des menus
	this.dataBox=null;

	this.description=(options.description!=undefined)?options.description:'';
	this.ver=(options.ver!=undefined)?options.ver:0;
	this.url=(options.url!=undefined)?options.url:null;//site du code source
	this.deb=this.date.getTime();
	this.fin=null;	this.dur=null;
	if (this.isConsole)	{this.setConsole();}

	this.instances=new Array();//liste des instances de la lib

	this.erreurTranslate();//en fin car neccesite variable

	return this; //renvoie un pointeur
	}

gestionLibrairie_lib.prototype=
	{
	erreurTranslate:function()
		{
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
		this.erreurTxt['fr'][1]='non charger';
		this.erreurTxt['fr'][-1]='nom non defini';
		this.erreurTxt['fr'][-10]='div '+this.idHTML+'_Support non defini';
		this.erreurTxt['fr'][-11]='span '+this.idHTML+'_Nom non defini';
		this.erreurTxt['fr'][-12]='div/textarea '+this.idHTML+'_Texte non defini';
		}
	,erreurShow:function(instNu,errNu,lang)
		{
		return this.erreurTxt[lang?lang:'fr'][errNu?errNu:this.erreurNo];
		}

	,destruct:function(){}
	,end:function(){this.date=new Date();this.fin=this.date.getTime();this.dur=this.fin-this.deb;}
	
	// ajoute le param ds la liste si object et renvoie 1 sinon eznvoie 0
	,instanceAdd:function(instPtr)
		{
		if(typeof(instPtr)==='object'){this.instances[this.instances.length]=instPtr;return 1;}
		return 0;
		}
	,ongletAdd:function(f){
		if(typeof(f)!='object')return null;
	
		// --  creation de la div this.creerCSS(); -- //
		var idCSS=document.createElement('span');
		this.idCSSMenus.appendChild(idCSS);

		// ==  initilialisation de creation == //
		idCSS.id=f.idHTML;
		idCSS.className="gestLibNom gestLibOnglet";
		idCSS.innerHTML=f.texte;
		//		idCSS.style.position='absolute';
//		idCSS.style.overflow='hidden';
//		idCSS.style.top=0;
//		idCSS.style.zIndex=this.zIndex;
//		idCSS.style.height =this.hauteur+'px';
		idCSS.style.cursor='pointer';
//		idCSS.style.opacity=this.opacity;
		return idCSS;
	}

	,setConsole:function(genre){
		// - support - //
		this.idCSSSup=document.getElementById(this.idHTML+'Console');	if (this.idCSSSup==null){this.erreurNo=-10;return null;}
//		this.consoleGenre=this.idCSSSup.tagName.toUpperCase(); // DIV, TEXTAREA
//		g=(typeof(genre)=='string')?genre.toUpperCase():'DIV';
	//	this.consoleGenre=(typeof(genre)!=)genre.toUpperCase();

		// - creation du support des menus - //
		this.idCSSMenus=document.createElement('div');
		this.idCSSMenus.id=this.idHTMLMenus;	// utile?
		this.idCSSSup.appendChild(this.idCSSMenus); 

		// -- onglet:nom(show|hide) -- //
		var f={"idHTML":this.idHTML+"_nom","texte":"console:"+this.nom};
		this.idCSSNom=this.ongletAdd(f);
		this.idCSSNom.onclick=function(elt){
			var CSSId=elt.currentTarget.parentElement.parentElement.children[1].style; // attention ordre de l'elt code en DUR!!!
			CSSId.display=(CSSId.display=='block')?'none':'block';
		}

		// -- onglet:clear(efface le contenu) -- //
		var f={"idHTML":this.idHTML+"_clear","texte":"clear"};
		this.idCSSClr=this.ongletAdd(f);
		this.idCSSClr.onclick=function(elt){
			var CSSId=elt.currentTarget.parentElement.parentElement.children[1]; // atention ordre de l'elt code en DUR!!!
			switch (CSSId.tagName.toUpperCase()){ 
				case 'TEXTAREA':CSSId.value="";break;
				case 'DIV':default:CSSId.innerHTML="";break;
			}
		}

		// - creation de la dataBox - //
		this.dataBox=new gestionLibrairie_dataBox({"idCSSParent":this.idCSSSup,"genre":genre});
		this.dataBox.create();
		//		this.idCSSTxt=document.getElementById(this.idHTML+'Texte');	if (this.idCSSTxt==null){this.erreurNo=-12;}


		// -- onglet:eval(si genre TEXTAREA) : neccessite que la dataBox soit creer! -- //
		if(this.dataBox.genre=='TEXTAREA'){
			var f={"idHTML":this.idHTML+"_eval","texte":"eval"};
			this.idCSSEval=this.ongletAdd(f);
			this.idCSSEval.onclick=function(elt){
				var data=elt.currentTarget.parentElement.parentElement.children[1].value; // attention ordre de l'elt code en DUR!!!
				eval(data);
				}
		}

		if (!this.erreurNo){this.idCSSNom.innerHTML='console:'+this.nom;}
		this.dataBox.write("console activée");

		//	if (!this.isVisible){this.hide();}
		this.isConsole=1;
	}

	,valueToString:function(val){
		switch (typeof(val)){
			case 'undefined':return 'undefined';
			case null: return 'null';
		}
		return val;
	}

	,evaluer:function()
		{
		if (this.idCSSTxt)
			{switch (this.idCSSTxt.tagName.toUpperCase())
				{
				case 'TEXTAREA':eval(this.idCSSTxt.value);break;
				case 'DIV':default:eval(this.idCSSTxt.innerHTML);
				break;
				}
			}
		}
	,write:function(o){this.dataBox.write(o);}
	
	// - inspect renvoie un texte avec le nom et la valeur d'une variable DANS la console - //
	,inspect:function(o){
		if(typeof(o)!='object')return null;
		var toV=typeof(o.varPtr);
		return this.dataBox.write({
//			"notWrite":1
			"TEXTAREA":'['+toV+'] '+o.varNom+'='+o.varPtr
			,"DIV":'<span class="gestLibVarNom">'+'['+toV+'] '+o.varNom+'</span>=<span class="gestLibVar">'+this.valueToString(o.varPtr)+'</span>'
		});
	}

	,inspectAll:function(o){
		var out='';
		o.notWrite=1;

		switch (typeof(o.varPtr)){
			case 'object':
//				if(this.dataBox.genre=='DIV')out+='<span class="gestLibVal">';
				// - affichage du nom de la variable - //
				if (o.varNom){
					out+=this.dataBox.write({
						"notWrite":1
						,"TEXTAREA":o.varNom+'='+o.varPtr
						//,"DIV":'<span class="gestLibVarNom">'+o.varNom+'</span>=<span class="gestLibVar">'+this.valueToString(o.varPtr)+'</span>'
						,"DIV":'<span class="gestLibVarNom">'+o.varNom+'</span>'
					});
				}

				if(this.dataBox.genre=='DIV')out+='<ul>';
				for (var v in o.varPtr){
					var to=typeof(o.varPtr[v]);
					out+='--'+to+'--';
					//if(typeof(v)==undefined)continu0e;
					if(this.dataBox.genre=='DIV')out+='<li>'; else out+=' * ';
					out+=this.inspectAll({"lib":o.lib,"varNom":v,"varPtr":o.varPtr[v],"notWrite":1});
					if(this.dataBox.genre=='DIV')out+="</li>";else out+="\n";
				}
				if(this.dataBox.genre=='DIV')out+='</ul>';
				break;
			default:
					if(this.dataBox.genre=='DIV')out+='<li>'; else out+=' * ';
					out+=this.inspect(o.varPtr);
					if(this.dataBox.genre=='DIV')out+="</li>";else out+="\n";

//				out+=this.valueToString(o.varPtr);
			}
//		if(this.dataBox.genre=='DIV')out+='</span><br>';
//		this.write(out);
		return out;
	}

}//gestionLibrairie_lib.prototype


/* ************************************************************
 * gestionLibrairie
 *
***********************************************************/
function gestionLibrairie(){
	this.erreurNo=0;this.erreurTxt=new Array();//attention tableau de tableau
	this.libNb=0;
	this.libs=new Array();
	return this;
}

gestionLibrairie.prototype=
	{
	////////////////////////////////
	//Gestion des erreurs
	erreurTranslate:function(){
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
	}
	,erreurShow:function(errNu,lang){
		var out='';
		for(libNu in this.libs)
			{
			if(typeof(this.libs[libNu].erreurShow)==='function')
				{
				out+=this.libs[libNu].nom+':'+this.libs[libNu].erreurShow(errNu,lang)+' ';
				}
			}
		return out;
	}

	// - creation d'une instance- //
	,instanceAdd:function(libNom,instPtr){
		var lib=this.libs[libNom];
		if(lib)lib.instanceAdd(instPtr);
	}
		
	////////////////////////////////
	,destruct:function(lib)
		{
		document.write('destruct:function(lib)<br>');
		// - lib preciser - //
		if(libNom){// -- poui: on la detruit -- //
			var lib=this.libs[libNom];
			if(lib){
				//document.write('===>this.libs[lib]:'+this.libs[lib]+'<br>');
				lib.destruct();
				lib=undefined;
				this.libNb--;
				}
		}
		else{ // -- non: on les detruits toutes -- //
			var libNu=0;
			for(libr in this.libs)
				{
//				document.write('<b>===->this.libs[libr].nom:'+this.libs[libr].nom+'</b><br>');
//				document.write('===>this.libs[libr]:'+this.libs[libr]+'<br>');
//				document.write('=== >this.libs[libr].typeof:'+this.libs[libr].typeof+'<br>');
//				this.libs[libr].destruct();
				this.libs[libr]=undefined;
				libNu++;
				if (libNu>=this.libNb){break;}
				}
			this.libs=undefined;
			this.libNb=0;
		}
	},

	////////////////////////////////
	//gestion des consoles
	end:function(libNom)
		{var lib=this.libs[libNom];if (lib)lib.end();}

	,setConsole:function(libNom,genre)
		{var lib=this.libs[libNom];if (lib)lib.setConsole(genre);}

	,clear: function(libNom)
		{var lib=this.libs[libNom];if (lib && lib.isConsole) return lib.clear();}

	,evaluer:function(o)
		{var lib=this.libs[o.lib];if (lib && lib.isConsole) lib.evaluer();}

	,write: function(o)
		{var lib=this.libs[o.lib];if (lib && lib.isConsole) return lib.write(o);}

	,inspect:function(o)
		{var lib=this.libs[o.lib];if(lib && lib.isConsole)return lib.inspect(o);}

	,inspectAll:function(o)
		{var lib=this.libs[o.lib];if(lib && lib.isConsole)return lib.inspectAll(o);}

	,loadLib:function(options)
		{
		if (!options){return -1;}
		if (!options.nom){return -2;}
		if(this.libs[options.nom]==undefined ||options.force)
			{
			this.libs[options.nom]=new gestionLibrairie_lib(options);
			if (this.libs[options.nom].erreurNo===0){return this.libs[options.nom];}
			this.libNb++;
			return 0;		
			}
		return -3;
		}
	,display:function(libNom,etat)	{var lib=this.libs[libNom];if(lib.isConsole)lib.display(etat);}
	,switchShow:function(libNom)	{var lib=this.libs[libNom];if(lib.isConsole)lib.switchShow();}
	,show:function(libNom)		{var lib=this.libs[libNom];if(lib.isConsole)lib.show();}
	,hide:function(libNom)		{var lib=this.libs[libNom];if(lib.isConsole)lib.hide();}

	////////////////////////////////
	,tableau:function()
		{
		var out='';
		out+='<table class="gestLib"><caption>Librairies JavaScript ('+this.libNb+')</caption>';
		out+='<thead><tr><th>type</th><th>nom</th><th>version</th><th>err</th><th>dur&eacute;e</th>';
		out+='<th>description</th><th>url</th>';
		out+='<th title="console demander?">console</th><th title="console visible?">visible</th><th>idHTML</th><th>instances</th></tr></thead>';

		for(value in this.libs)
			{if (this.libs[value].nom)
				{
				url='';
				if (this.libs[value].url!=null){url='<a target="git" href="'+this.libs[value].url+'">lien</a>';}
				out+='<tr><td>'+this.libs[value].libType+'</td><td>'+this.libs[value].nom+'</td><td>'+this.libs[value].ver+'</td>';
				out+='<td>'+this.libs[value].erreurNo+':'+this.libs[value].erreurShow()+'</td>';
				out+='<td>'+this.libs[value].dur+'</td>';
				out+='<td>'+this.libs[value].description+'</td><td>'+url+'</td>';
				out+='<td>'+this.libs[value].isConsole+'</td><td>'+this.libs[value].isVisible+'</td><td>'+this.libs[value].idHTML+'</td>';
				out+='<td>';
	
				//erreurs liee  aux instances
				var instout='';
				for(instNu in this.libs[value].instances)
					{instout+='<b>'+instNu+':</b>';
					if(typeof(this.libs[value].instances[instNu].erreurShow)==='function')
						{instout+=this.libs[value].instances[instNu].erreurShow();}
					else	{instout+='pas de function erreurShow()';}
					instout+='<br>';
					}
				if(instout!='')out+=instout;
				out+='</td>';
				out+='</tr>\n';
				}
			};
		out+='</table>';
		return out;
		}
	} //class gestionLibrairie prototype

/* ************************************************************
instantiation du gestionnaire
 ***********************************************************/
gestLib=new gestionLibrairie();
gestLib.loadLib({nom:'gestLib'/*,idHTML:'gestLib'*/,ver:GESTLIBVERSION,description:'gestionnaire de librairies js',isConsole:0,isVisible:0,url:'https://git.framasoft.org/legraLibs/gestlib'});
gestLib.instanceAdd('gestLib',gestLib);

gestLib.end('gestLib');

/*! - /www/git/intersites/lib/legral/js/gestClasseurs/gestClasseurs.js - */
/*!
fichier: gestClasseurs.js
auteur:pascal TOLEDO
date de creation : 2014.12.05
date de modification: 2015.04.20
depend de:
  * rien
description: cf README
*/

GESTCLASSEURSVERSION= '0.2.2-dev';

if(typeof(gestLib)==='object')gestLib.loadLib({nom:'gestClasseurs',ver:GESTCLASSEURSVERSION,description:'creation de div dynamique en js',isConsole:0,isVisible:0,url:'https://git.framasoft.org/legraLibs/gestclasseurs'});


//options: nom:obligatoire
//erreur:
// -1: absence d'options
// -10: la requete Ajax a echouer


/*
 * function TclasseursFeuille(classeurNom,options)
 * param obligatorie:
 * nom: pas d'espace ni de caractere spéciaux (sert a calcule l'IDHTML des enfants)
 * classeurIDJS
 */
function TclasseursFeuille(f){
	this.methodRun='TclasseurFeuille';
	this.isLoad=0;
	if(typeof(f)!='object')return -1;
	this.nom=f.nom;				// pas d'espace ni de caracteres spéciaux (sert a calcule l'IDHTML des enfants)

	// - - //
	this.classeurIDJS=f.classeurIDJS;
	this.description=f.description;	// description de la feuille

	// -  menu: support des onglets - //
	this.menuIDCSS=this.classeurIDJS.menuIDCSS;
	this.ongletIDCSS=null;
	this.titre=(f.titre)?f.titre:f.nom;	// titre de l'onglet (texte afficher)

	// - datasBoxSupport - //
	this.datasBoxSupportIDCSS=this.classeurIDJS.datasBoxSupportIDCSS;
	this.datasBoxIDCSS=null;	// - IDCSS de la dataBox contenant le texte
	this.texte=f.texte;			// -  contenu de la dataBox
	//	this.liidCSS=null;

	this.description=f.description;

	// - creation de la feuille: (onglet + dataBox) - //
	this.loadFeuille();
	this.isLoad=1;

	if(f.hide===1)this.hide();	// cacher la feuille si demandé
	return this;
} // class Tclasseur


TclasseursFeuille.prototype={
	destruct:function()
		{
		document.getElementById(this.support+'_li').remove();
		document.getElementById(this.support+'_datas').remove();
		this.feuille=undefined;
		}
	,onClick:function(elt,f){
	}

	,loadFeuille:function(){
		this.methodRun='TclasseurFeuille:loadFeuille';

		// - creation de l'onglet dans le menu - //
		this.ongletIDCSS=document.createElement('span');
		this.ongletIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_onglet';	// utile?
		this.ongletIDCSS.className="gestClasseurs_onglet";
		this.ongletIDCSS.innerHTML=this.titre;
		this.menuIDCSS.appendChild(this.ongletIDCSS);

		var _classeurIDJS=this.classeurIDJS; // pour envoyer dans la fonction anonyme
		var _feuilleIDJS=this;

		// - activation du click sur le menu - //
		this.ongletIDCSS.onclick=function(elt){
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_classeurIDJS.nom","varPtr": _classeurIDJS.nom});
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_feuilleIDJS.nom","varPtr":  _feuilleIDJS.nom});
				_classeurIDJS.select(_feuilleIDJS.nom);
				
				}



		// - creation de la dataBox dans la datasBoxSupport - //
		this.datasBoxIDCSS=document.createElement('div');
		this.datasBoxIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_datasBox';	// utile?
		this.datasBoxIDCSS.className="gestClasseurs_datasBox";
		this.datasBoxIDCSS.innerHTML=this.texte;
		this.datasBoxSupportIDCSS.appendChild(this.datasBoxIDCSS);


	} // loadFeuille

	// - selectionne l'onglet: le met au 1er plan et applique le style - //
	,select:function(){
		// -- mise en style -- //
		this.ongletIDCSS.className+=' gestClasseurs_ongletSelected';
		this.datasBoxIDCSS.style.display='block';

	}

	,unselect:function(){
		// -- supprime le style -- //
		this.ongletIDCSS.className=this.ongletIDCSS.className.replace(' gestClasseurs_ongletSelected','');
		this.datasBoxIDCSS.style.display='none';
	}

	// - affiche l'onglet et la dataBox - //
	,show:function(){
		this.ongletIDCSS.style.display='inline';
		this.datasBoxIDCSS.style.display='block';
	}

	// - rend invisible l'onglet et la box - //
	,hide:function(){
		this.ongletIDCSS.style.display='none';
		this.datasBoxIDCSS.style.display='none';
		}

	,write:function(texte){this.datasBoxIDCSS.innerHTML=texte;}

	,append:function(texte){this.datasBoxIDCSS.innerHTML+=texte;}
}

/*********************
 *
 *
 ********************/

function Tclasseur(c)
	{
	this.erreur=0;
	this.methodRun='Tclasseur';
	if(typeof(c)!='object'){this.erreur=-1;return -1;}
	if(!c.nom){this.erreur=-2;return -2;}
	this.nom=c.nom;

	// - support general - //
	this.htmlID=(c.htmlID)?c.htmlID:c.nom;
	this.htmlIDCSS=document.getElementById(this.htmlID);
	if(!this.htmlIDCSS){this.erreur=-3;return -3;}


	// - support du menu des onglets - //
	this.menuIDCSS=null;
	this.datasBoxSupportIDCSS=null;
	this.menuTitre=c.menuTitre?c.menuTitre:'';

	// - - //
	this.feuilleNb=0;
	this.feuilleDetruiteNb=0;
	this.feuilles=new Array();
	this.creerSupport();
	return this;
	}//class Tclasseur

Tclasseur.prototype ={
	destruct:function(feuille){
		if(feuille)
			{if(this.feuilles[feuille]!=undefined&&'unload')
				{this.feuilles[feuille].destruct();this.feuilles[feuille]='unload';this.feuilleDetruiteNb++;}
				}
		else	{
			var feuilleNu=0;
			for(feuiller in this.feuilles)
				{
				this.feuilles[feuiller].destruct();
				this.feuilles[feuiller]='unload';
				feuilleNu++;
				if(feuilleNu>=this.feuilleNb){break;}
				}
			this.feuilles='unload';
			this.feuilles=new Array();
			this.feuilleDetruiteNb=0;
			this.feuilleNb=0;
			}
	},

	clean:function(nom){var temp=new Array();
		for (var key in this.feuilles)
			{
			if(this.feuilles[key]!='unload'&&key!=nom){temp[key]=this.feuilles[key];}
			this.feuilleNb--;
			}
		this.feuilles=temp;
	},
		
	creerSupport:function(){

		// - creation du support des menus - //
		this.menuIDCSS=document.createElement('div');
		this.menuIDCSS.id=this.nom+'_onglets';	// utile?
		this.menuIDCSS.className="gestClasseurs_menuSupport";
		this.menuIDCSS.innerHTML=this.menuTitre;
		this.htmlIDCSS.appendChild(this.menuIDCSS); 

		// - creation du support des dataBox - //
		this.datasBoxSupportIDCSS=document.createElement('div');
		this.datasBoxSupportIDCSS.id=this.nom+'_datasBoxSupport';	// utile?
		this.datasBoxSupportIDCSS.className="gestClasseurs_datasBoxSupport";
		this.datasBoxSupportIDCSS.innerHTML="datasBoxSupport";
		this.htmlIDCSS.appendChild(this.datasBoxSupportIDCSS); 
	}

	,loadFeuille:function(f){
		this.methodRun='Tclasseur';
		this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}
		if(!f.nom){this.erreur=-2;return -2;}		// la feuille n'a pas de nom
		
		if(this.feuilles[f.nom]==undefined||f.forcer){
			this.destruct(f.nom);

			f.classeurIDJS=this;

			this.feuilles[f.nom]=new TclasseursFeuille(f);

			// - supprimer la feuille en cas d'erreur - //
			if(this.feuilles[f.nom].erreur<0){
				this.feuilleNb--;
				erreur=this.feuilles[f.nom].erreur;
				this.destruct(this.f.nom);
				return erreur;
			}
		}

	}// loadFeuille()

	// - met la feuille au 1er plan (ne l'affiche pas si elle est caché)  - //
	,select:function(nom){
		var f=this.feuilles[nom];
		if(f != undefined) {
			this.unselectAll();
			f.select();
		}
	}
	
	,unselectAll:function(){
		for(var key in this.feuilles){
			var f=this.feuilles[key];
			if(f && f.nom)
				{f.unselect();}
		}

	}

	// - rend visible l'onglet et la dataBox - //
	,show:function(nom){
		// - si aucun nom de feuille n'est donnee alors les afficher toutes - //
		if(!nom){
			for(var key in this.feuilles)
				{
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].show();}
				}
		}
		else{
			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].show();}
		}
	}

	,hide:function(nom){
		// - si aucun nom de feuille n'est donnee alors les cacher toutes - //
		if(!nom){
			for(var key in this.feuilles)
				{
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].hide();}
				}
		}
		else{
			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].hide();}
		}
	},

	write:function(f){
		this.methodRun='Tclasseur:write';this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}

		var fe=this.feuilles[f.feuille];
		if(fe != undefined && fe != 'unload')
			{fe.write(f.texte);}
	}

	,append:function(f){
		this.methodRun='Tclasseur:append';this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}

		var fe=this.feuilles[f.feuille];
		if(fe != undefined && fe != 'unload')
			{fe.append(f.texte);}
	}

} // Tclasseur.prototype



// - enregistrement de la librairie dans le gestionanire de librairie - //
if(typeof(gestLib)==='object')gestLib.end('gestClasseurs');
