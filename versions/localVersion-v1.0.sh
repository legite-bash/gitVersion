#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier


# - on se positionne sur la racine du projet - #
# - code obligatoirement dans ce script aussi - #
cd $scriptRep/../


function localSave {



	# - CSS - #

	# - concatenation des fichiers css - #
	catCSS /www/sites/intersites/lib/tiers/css/knacss/knacss.css
	catCSS /www/git/intersites/lib/legral/php/gestLib/versions/gestLib-0.1.css
	

	# - concatenation des fichiers js - #

	catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js 


	# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
	versionSave ./scripts/gitVersion sh
	versionSave ./localVersion sh

	#exemple:
	#versionSave script js

	# - fichier (javascript uniquement) a compresser et a mettre dans ./versions) - #
	#versionMinimise script js 
	#versionMinimise script2 js
	}


