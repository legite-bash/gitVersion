#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier

# - configuration - #

localConcat() {
        # - concatenation des fichiers css - #
        echo "$couleurINFO # - concatenation des fichiers css - #$couleurNORMAL";
#        catCSS ./styles/knacss.css
#        catCSS ./styles/gestLib-0.1.css

#        catCSS ./styles/html4.css
#        catCSS ./styles/intersites.css

#        catCSS ./styles/notes/notesLocales.css
#        catCSS ./styles/menuOnglets-defaut/menuLocales.css
#        catCSS ./styles/tutoriels/tutorielsLocales.css


        # - concatenation des fichiers js - #
        echo "$couleurINFO # - concatenation des fichiers js - #$couleurNORMAL";
        #catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js

        # - cp de pages - #
        echo "$couleurINFO # - copie de pages - #$couleurNORMAL";
        cp -R /www/0pages/_site/ ./pagesLocales/_site/

	}

localSave() {
	# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
	versionSave ./scripts/gitVersion sh
	versionSave ./localVersion sh

	#exemple:
	#versionSave script js

#	if [ -f './scripts/statique.sh' ];then
#		./scripts/statique.sh
#		fi
	}


#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	echo "$couleurINFO # - localVersion.sh:syncRemote() - #$couleurNORMAL";
	# - http://doc.ubuntu-fr.org/lftp - #
	# configurer ~.netrc pour ne pas a avoir a mettre le passsword

	# repertoire local ./repereTemporel sera (ftp.legral.fr/ /rt/repereTemporel
	# if [ $isSimulation -eq 0 ];then
	#lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./repereTemporel   /rt/ ; quit"
	# fi
}

#################
# postGit()     #
# lancer en fin #
#################
postGit() {
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	# if [ $isSimulation -eq 0 ];then
	# fi
	}
