#!/bin/sh
VERSION="0.2";
scriptPath=$0;           #chemin complet du script 
scriptRep=`dirname $0`;  #repertoire du script
localVersionPath="./localVersion.sh";

# - chargement des codes couleurs - #
source $scriptRep/couleur.sh

echo "$couleurINFO$repScript/gitVersion v$VERSION";

# - usage si pas de parametre - #
if [ $# -lt 2 ];then
#	echo "$couleurINFO$repScript/gitVersion v$VERSION";
	echo "usage:"
	echo " gitVersion version commit";
	echo " gitVersion version push";
	echo " gitVersion version commit push";
	exit
	fi


# - genrere les fichiers contennue dans ./locales/ ) - #
function localGenMin {
	# - minimise scripts.js - # 
	# yuicompressor rajoute automatiquement l'extention ".js|.css" (pas ici! -> rep inside ?)

	echo "compression de ./locales/scripts.js .$couleurNORMAL";
	java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./locales/scripts.js -o ./locales/scripts.min.js
	echo "suppression de ./locales/scripts.js .$couleurNORMAL";
	rm ./locales/scripts.js

	echo "compression de ./locales/styles.css .$couleurNORMAL";
	java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./locales/styles.css -o ./locales/styles.min.css
	echo "suppression de ./locales/styles.css .$couleurNORMAL";
	rm ./locales/styles.css
        }

function commit {

	echo "$couleurINFO on commit...";
	echo "mais avant:";
	echo "1- on copie la version dans le rep version.";
	if [ -f "$localVersionPath" ];
	then
		echo "fichier $localVersionPath trouve.$couleurNORMAL";
		source $localVersionPath
		localSave;
		localGenMin;
	else
		echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
		fi

	echo "$couleurINFO 2- on add .$couleurNORMAL";
	git add .

	echo "$couleurINFO maintenant on commit (copier le texte issue de release)$couleurNORMAL";
	git commit

	echo "$couleurINFO on tag le commit avec $couleurWARN$PROGVERSION$couleurNORMAL";
	git tag $PROGVERSION;
	}

function push {
	echo "$couleurINFO on push les commits$couleurNORMAL";
	git push;
	echo "$couleurINFO on push les tags$couleurNORMAL";
	git push --tag
	exit;
	} 

function sortieOk {
	echo "$couleurINFO on affiche le status:$couleurNORMAL"
	git st;
	echo "$couleurINFO on affiche les tags$couleurNORMAL";
	git tag;
	}



############################################
# - functions utiliser par localesSave() - #
############################################

# - concatenation de fichier - #
function catJS {
	echo "/*! $1 */" >> ./locales/scripts.js
        cat "$1"            >> ./locales/scripts.js
        }

function catCSS {
	echo "/*! $1 */" >> ./locales/styles.css
        cat "$1"            >> ./locales/styles.css
        }


# - creer une copie d'un js|css en ajoutant la version en suffixe - #
# %1:fn:fichier nom
# %2:fe: fichier extention (sans le point)
# (calc)fv: nom du fichier avec la version en suffixe
function versionSave {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$PROGVERSION";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	}

# - minimise les fichiers js ou css (selon $2) - #

# minSuf: suffixe de la minification
function versionMinimise {
	fn=$1;
	fe=".$2";
        fv="$fn-$PROGVERSION";
	minSuf=".min";fm="$fv$minSuf$fe";
        echo "compression de  $fn$fe vers $fm$fe";
	#java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm$fe
	#yuicompressor rajoute automatiquement l'extention ".js|.css"
	java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm
        }


########
# main #
########
PROGVERSION=$1;

# - on se positionne sur la racine du projet - #
cd $scriptRep/../
echo "$couleurINFO repertoire courant $couleurNORMAL"
pwd


# - Creation des repertoires et fichiers - #

rep="./.git";
if [ -d "$rep" ];
then
	echo "$couleurINFO repertoire $rep existant$couleurNORMAL"
else
	echo "$couleurWARN repertoire $rep NON existant -> creation d'un repo initial$couleurNORMAL"
	git init;
	fi

rep="./versions";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL"
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL"
	mkdir $rep
        fi


rep="./locales";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL"
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL"
        mkdir $rep
        fi

# - creation/remise a zero  des fichiers locales - #
echo "/*! fichier generer automatiquement et compresser avec yuicompressor */" > $rep/styles.css
echo  "/*! fichier generer automatiquement et compresser avec yuicompressor */" > $rep/scripts.js
	


# - execution des commandes selectionnees - #

if [ "$2" == "commit" ];then
	commit;
	if [ "$3" == "push" ];then
		push;
		fi
	sortieOk;
	fi

if [ "$2" == "push" ];then
	push;
	sortieOk;
	fi

