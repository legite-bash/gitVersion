#!/bin/sh
# - configuration - #
VERSION="1.1.1";
noStatus=0;             # si 1: git status ne sera pas lancer

# - variable interne - #
scriptPath=$0;           #chemin complet du script 
scriptRep=`dirname $0`;  #repertoire du script
localVersionPath="./localVersion.sh";
islocalVersionLoad=0;
isTest=0;		# si version == 'test' ->ni commit ni push
commitFail=0;		# egale a 1 si le commit a echoue



# - chargement des codes couleurs - #
source $scriptRep/couleur.sh

echo "$couleurINFO$repScript gitVersion v$VERSION$couleurNORMAL";

# - usage si pas de parametre - #
if [ $# -lt 1 ];then
	echo "usage:"
	echo " ./scripts/gitVersion.sh$couleurINFO version$couleurNORMAL min; # concat et minimise les js et css";
	echo " ./scripts/gitVersion.sh$couleurINFO version$couleurNORMAL commit; # min + add + commit";
	echo " ./scripts/gitVersion.sh on push; # push + tag avec 'version'";
	echo " ./scripts/gitVersion.sh$couleurINFO version$couleurNORMAL commit push";
	echo " $couleurWARN si$couleurINFO version$couleurWARN = 'test' le commit et le push n'est pas reellement fait.$couleurNORMAL";
	exit
	fi


# - genrere les fichiers contennue dans ./locales/ ) - #
# - generation de script.js et styles.css - #
function localGenMin {

	# - chargement config local - #
	if [ $islocalVersionLoad == 1 ];
	then

		# - concatenation des fichiers - #
		echo "$couleurINFO localConcat: concatenation des fichier.$couleurNORMAL";
		localConcat;	# cette fonction est dans le fichier $localVersionPath 

		# - minimise scripts.js - # 
		# yuicompressor rajoute automatiquement l'extention ".js|.css" (pas ici! -> rep inside ?)

		echo "compression de ./locales/scripts.js .$couleurNORMAL";
		java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./locales/scripts.js -o ./locales/scripts.min.js
#		echo "suppression de ./locales/scripts.js .$couleurNORMAL";
#		rm ./locales/scripts.js

		echo "compression de ./locales/styles.css .$couleurNORMAL";
		java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./styles/styles.css -o ./styles/styles.min.css
#		echo "suppression de ./locales/styles.css .$couleurNORMAL";
#		rm ./locales/styles.css
		fi
        }

function commit {
	echo "$couleurINFO on commit...";
	echo "mais avant:";
	echo "1a- on copie la version dans le rep version.";
	if [ $islocalVersionLoad == 1 ];
	then
		# - generation de script.js et styles.css - #
		localGenMin;	# appele source $localVersionPath neccessaire a localSave()

		# - creation des versions et copies dans ./versions - #	
		localSave;	# cette fonction est dans le fichier $localVersionPath 

	else
		echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
		fi


	# - mise a jours du fichier contenant la version - #
	if [ $isTest == 0 ];then
		echo "$couleurINFO mise a jours du fichier lastVersion$couleurNORMAL";
		echo "$PROGVERSION" > ./lastVersion
		fi

	# - On add les fichiers - #
	echo "$couleurINFO 2- on add .$couleurNORMAL";
	git add .

	# - on commit  - #
	if [ $isTest == 0 ];then
		echo "$couleurINFO maintenant on commit (copier le texte issue de release)$couleurNORMAL";
		git commit

		if [ $? == 0 ];then
			echo "$couleurINFO on tag le commit avec $couleurWARN$PROGVERSION$couleurNORMAL";
			git tag $PROGVERSION;
		else
			echo "$couleurWARN git commit a renvoyer le code d'erreur $gitErreur";
			commitFail=1;
			fi
	else
		echo "$couleurWARN mode test: pas de commit ni tag $couleurNORMAL";
		fi
	}


function push {
	if [ $isTest == 0 ];then
		echo "$couleurINFO on push les commits$couleurNORMAL";
		git push;
		echo "$couleurINFO on push les tags$couleurNORMAL";
		git push --tag
	else
		echo "$couleurWARN mode test: pas de push $couleurNORMAL";
		fi		

	} 

function sortieOk {
	if [ $noStatus == 0 ];then
		echo "$couleurINFO on affiche le status:$couleurNORMAL"
		git status;
		fi
	echo "$couleurINFO on affiche les tags$couleurNORMAL";
	git tag;
	exit 0;	#on quitte le script
	}



###########################################
# - functions utiliser par localesSave() - #
############################################

# - concatenation de fichier - #
function catJS {
	echo -e  "\r\n/*!                      $1 */" >> ./locales/scripts.js
	echo "$couleurWARN";
        cat "$1"            >> ./locales/scripts.js
	echo "$couleurNORMAL";
        }

function catCSS {
	echo -e  "\r\n/*!                      $1 */" >> ./styles/styles.css
	echo "$couleurWARN";
        cat "$1"            >> ./styles/styles.css
	echo "$couleurNORMAL";
        }


# - creer une copie d'un js|css en ajoutant la version en suffixe - #
# %1:fn:fichier nom
# %2:fe: fichier extention (sans le point)
# (calc)fv: nom du fichier avec la version en suffixe
function versionSave {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$PROGVERSION";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	}

# - minimise les fichiers js ou css (selon $2) - #

# minSuf: suffixe de la minification
function versionMinimise {
	fn=$1;
	fe=".$2";
	minSuf=".min";
	fm="$fn$minSuf-$PROGVERSION$fe";
        echo "compression de  $fn$fe vers $fm$fe";
	java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm
        }


########
# main #
########
PROGVERSION=$1;

# - on se positionne sur la racine du projet - #
cd $scriptRep/../
echo "$couleurINFO repertoire courant: $couleurNORMAL";
pwd;


# - push only - #
if [ "$1" == "push" ];then
	echo "$couleurINFO push only$couleurNORMAL";
	push;
	sortieOk;
	fi

# - Creation des repertoires et fichiers - #

rep="./.git";
if [ -d "$rep" ];
then
	echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
	echo "$couleurWARN repertoire $rep NON existant -> creation d'un repo initial$couleurNORMAL";
	git init;
	echo "$couleurWARN taper 'git push --set-upstream origin master' pour le positionner sur master $couleurNORMAL";
	fi

rep="./versions";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	mkdir $rep
        fi


rep="./locales";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	mkdir $rep
        fi

rep="./styles";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
        mkdir $rep
        fi

rep="./pages";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
        mkdir $rep
	echo  "pages specifique a ce projet. Les pages communes sont dans ./0pages/ (repertoires montes depuis /www/0pages/" > $rep/readme
        fi

rep="./0pages";
if [ -d "$rep" ];
then
        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
else
        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	mkdir $rep
	fi

echo "montage de /www/0pages dans ./0pages/";
fichier="./0pages/0pages";
if [ -f "$fichier" ];
then
	echo "$couleurINFO presence de $fichier detecte: le rep est considere comme deja monte";
else
	echo "si la fonction ne marche pas modifier /etc/sudoers$couleurNORMAL";
	sudo mount --bind /www/0pages $rep
	fi


# - creation/remise a zero des fichiers locales - #
echo "$couleurINFO creation/remise a zero des fichiers locales $couleurNORMAL";
echo "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./styles/styles.css
echo  "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./locales/scripts.js


# - chargement de la configuration utilisateur  - #
if [ -f "$localVersionPath" ];
then
	echo "$couleurINFO Chargement de $localVersionPath $couleurNORMAL";
	source $localVersionPath;
	islocalVersionLoad=1;
else
	echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
	islocalVersionLoad=0;
	fi


# - lecture des options  - #
# -- version == 'test'-- #
if [ "$1" == "test" ];then
	isTest=1;
	fi

# - execution des commandes selectionnees - #

if [ "$2" == "min" ];then
        localGenMin;
        sortieOk;
        fi


if [ "$2" == "commit" ];then
	commit;	# execute localGenMin();
	if [ "$3" == "push" ];then
		if [ $commitFail != 0 ];then
			echo "$couleurWARN Probleme lors du commit: pas de push $couleurNORMAL";
			fi		
		push;
		fi
	sortieOk;
	fi
